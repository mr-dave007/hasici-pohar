<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Pohar;

Route::get('/', [Pohar::class, 'index']);
Route::get('/vysledky', [Pohar::class, 'vysledky']);

Route::get('/admin', [Pohar::class, 'indexAdmin']);
Route::post('/admin/druzstvo/add', [Pohar::class, 'AdminDruzstvoAdd']); // Prida druzstvo
Route::get('/admin/druzstvo/del/{id}', [Pohar::class, 'AdminDruzstvoDel']); // Smaze druzstvo

Route::post('/admin/druzstvo/edit', [Pohar::class, 'AdminDruzstvoUpdate']); // Uprava vysledku druzstvo

Route::get('/admin/delall/{kategorie}', [Pohar::class, 'AdminDruzstvoDelAll']); // Smaze vsechny druzstva v kategorii

Route::get('/admin/results/{kategorie}/update', [Pohar::class, 'AdminResultsUpdate']);

// Route::post('/admin/discipline/edit', 'AdminDruzstvoUpdate');

Route::get('/admin/discipline/{discipline}/{kategorie}/update', [Pohar::class, 'AdminDisciplineUpdate']);

Route::get('/admin/export', [Pohar::class, 'AdminResults2Xlsx']);
Route::get('/admin/start-listina/export', [Pohar::class, 'AdminStartListina2Xlsx']);

Route::get('/4x60', [Pohar::class, 'index4x60']);
Route::post('/4x60', [Pohar::class, 'store4x60']);

Route::get('/dvojice', [Pohar::class, 'indexDvojice']);
Route::post('/dvojice', [Pohar::class, 'storeDvojice']);

Route::get('/utok', [Pohar::class, 'indexUtok']);
Route::post('/utok', [Pohar::class, 'storeUtok']);

Route::get('/kolo', [Pohar::class, 'indexKolo']);
Route::post('/kolo', [Pohar::class, 'storeKolo']);
