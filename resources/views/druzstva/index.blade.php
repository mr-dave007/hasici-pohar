@extends('layouts.app')

@section('title', 'Družstva')





@section('content')
    <div class="container">
        <h1 class="mb-2 text-center">Seznam druzstev</h1>

        @if(!empty($errors->first()))
            <div class="row col-lg-12">
                <div class="alert alert-danger">
                    <span>{{ $errors->first() }}</span>
                </div>
            </div>
        @endif

        @if(session('message'))
            <div class='alert alert-success'>
                {{ session('message') }}
            </div>
        @endif

        <div class="col-12 col-md-6">
            <form class="form-horizontal" method="POST" action="/druzstva">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="Druzstvo">Družstvo: </label>
                    <input type="text" class="form-control" id="AddDruzstvoName"
                            placeholder="Družstvo" name="AddDruzstvoName"
                            value="{{ old('AddDruzstvoName') }}"
                            required>
                </div>

                <div class="form-group">
                    <label for="Kategorie">Kategorie: </label>
                    <input type="text" class="form-control" id="AddDruzstvoKategorie"
                            placeholder="kategorie" name="AddDruzstvoKategorie"
                            value="{{ old('AddDruzstvoKategorie') }}"
                            required>
                </div>

                <div class="form-group">
                    <label for="message">Startovní číslo: </label>
                    <input type="text" class="form-controle"
                            id="AddDruzstvoStartCislo" placeholder="Startovní číslo"
                            name="AddDruzstvoStartCislo"
                            required value="{{ old('AddDruzstvoStartCislo') }}">
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary" value="Send">Uložit</button>
                </div>
            </form>
        </div>
    </div> <!-- /container -->
@endsection


