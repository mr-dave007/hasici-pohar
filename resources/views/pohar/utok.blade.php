@extends('layouts.app')

@section('title', 'DISCIPLÍNA - požární útok')


@section('content')
<br/>

<div class="tab-content" id="nav-tabContent">
@if ($kategorie=='mladsi')
<div id="mladsiTab" class="tab-pane fade in active show">
@else
<div id="mladsiTab" class="tab-pane fade in">
@endif
    <table id="tabMladsi" class="table table-striped table-bordered table-hover table-responsive-md text-nowrap">
        <thead class="thead-dark">
          <tr>
            <th class="th-sm text-center">START. ČÍSLO</th>
            <th class="th-sm">DRUŽSTVO</th>
            <th class="th-sm text-center">ČAS 1</th>
            <th class="th-sm text-center">ČAS 2</th>
            <th class="th-sm text-center">VÝSLEDNÝ ČAS</th>
            <th class="th-sm text-center">POŘADÍ</th>
          </tr>
        </thead>
        <tbody>
            @foreach($mladsi as $druzstvo)
                <tr>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['StartCislo'] }}</td>
                    <td>
                    @if ($druzstvo['VyslednyCas']==null and $druzstvo['Cas2']==null and $druzstvo['Cas1']==null)
                        <button type="button" class="btn btn-outline-red btn-sm m-0 waves-effect" onclick="ShowModalUtok('{{ $druzstvo['Druzstvo'] }}' + ' - mladší - pokus 1', {{ $druzstvo['id'] }}, 1);">{{ $druzstvo['Druzstvo'] }}</button>
                    @elseif ($druzstvo['Cas1']!=null and $druzstvo['VyslednyCas']==null)
                        <button type="button" class="btn btn-outline-primary btn-sm m-0 waves-effect" onclick="ShowModalUtok('{{ $druzstvo['Druzstvo'] }}' + ' - mladší - pokus 2', {{ $druzstvo['id'] }}, 2);">{{ $druzstvo['Druzstvo'] }}</button>
                    @else
                        <button type="button" class="btn btn-outline-default btn-sm m-0">{{ $druzstvo['Druzstvo'] }}</button>
                    @endif
                    </td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Cas1'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Cas2'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['VyslednyCas'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Poradi'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

@if ($kategorie=='starsi')
<div id="starsiTab" class="tab-pane fade active show"> 
@else
<div id="starsiTab" class="tab-pane fade"> 
@endif
    <table id="tabMladsi" class="table table-striped table-bordered table-hover table-responsive-md text-nowrap">
        <thead class="thead-dark">
          <tr>
            <th class="th-sm text-center">START. ČÍSLO</th>
            <th class="th-sm">DRUŽSTVO</th>
            <th class="th-sm text-center">ČAS 1</th>
            <th class="th-sm text-center">ČAS 2</th>
            <th class="th-sm text-center">VÝSLEDNÝ ČAS</th>
            <th class="th-sm text-center">POŘADÍ</th>
          </tr>
        </thead>
        <tbody>
            @foreach($starsi as $druzstvo)
                <tr>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['StartCislo'] }}</td>
                    <td>
                    @if ($druzstvo['VyslednyCas']==null and $druzstvo['Cas2']==null and $druzstvo['Cas1']==null)
                        <button type="button" class="btn btn-outline-red btn-sm m-0 waves-effect" onclick="ShowModalUtok('{{ $druzstvo['Druzstvo'] }}' + ' - starší - pokus 1', {{ $druzstvo['id'] }}, 1);">{{ $druzstvo['Druzstvo'] }}</button>
                    @elseif ($druzstvo['Cas1']!=null and $druzstvo['VyslednyCas']==null)
                        <button type="button" class="btn btn-outline-primary btn-sm m-0 waves-effect" onclick="ShowModalUtok('{{ $druzstvo['Druzstvo'] }}' + ' - starší - pokus 2', {{ $druzstvo['id'] }}, 2);">{{ $druzstvo['Druzstvo'] }}</button>
                    @else
                        <button type="button" class="btn btn-outline-default btn-sm m-0">{{ $druzstvo['Druzstvo'] }}</button>
                    @endif
                    </td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Cas1'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Cas2'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['VyslednyCas'] }}</td>
                    <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Poradi'] }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

</div>

<!-- MODAL OKNO -->
<!-- Modal -->
<div class="modal fade" id="UtokModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  method="POST" action="/utok">
            {{ csrf_field() }}
            <input type="hidden" id="druzstvoID" name="druzstvoID" value="0" />
            <input type="hidden" id="pokus" name="pokus" value="1" />
            <div class="modal-header">
                <h5 class="modal-title text-center font-weight-bold" id="ModalTitle">Chlumec nad Cidlinou - starší</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2">čas: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="xx:xx" pattern="[0-9]{2,4}:[0-9]{2}" id="cas" name="cas" required aria-label="Username" aria-describedby="basic-addon2">
                        </div>
                    </div>
                <div class="col">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="neplatny" name="neplatny"  onClick="if (this.checked) { $('#cas').val('N'); $('#cas').prop('disabled', true); } else { $('#cas').val('');$('#cas').prop('disabled', false); }">
                        <label class="custom-control-label" for="neplatny">Neplatný čas</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Uložit čas</button>
        </div>
        </form>
    </div>
</div>
@endsection
