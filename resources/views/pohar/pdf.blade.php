@extends('layouts.pdf')

@section('title', 'VÝSLEDKOVÁ LISTINA')


@section('content')

<br/>

<div class="tab-content">
        <table id="tabMladsi" class="table table-striped table-bordered table-hover table-responsive-lg text-nowrap">
            <thead class="thead-dark">
            <tr>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Start. číslo</th>
                <th class="align-middle font-weight-bold" rowspan="2" scope="col">Družstvo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Hadicové kolo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Dvojice</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">4x60m</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Útok</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Součet</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Pořadí</th>
            </tr>
            <tr>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
            </tr>
            </thead>
            <tbody>
                @foreach($kategorie as $druzstvo)
                    <tr>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['StartCislo'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Druzstvo'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['CasKolo'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['PoradiKolo'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['CasDvojice'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['PoradiDvojice'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Cas4x60'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['Poradi4x60'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['CasUtok'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['PoradiUtok'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['CelkovySoucet'] }}</td>
                        <td class="align-middle text-center font-weight-bold">{{ $druzstvo['CelkovePoradi'] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
</div>

@endsection

