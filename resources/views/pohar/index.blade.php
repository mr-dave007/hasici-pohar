@extends('layouts.app')

@section('title', 'VYBER DISCIPLÍNU')


@section('content')

<br/>

<div class="" align="center">
    <a href="4x60" class="text-white"><button type="button" class="btn btn-lg btn-primary">4x60m</button></a>
    <a href="dvojice" class="text-white"><button type="button" class="btn btn-lg btn-primary">Štafeta dvojic</button></a>
    <a href="utok" class="text-white"><button type="button" class="btn btn-lg btn-primary">Požární útok</button></a>
    <a href="kolo" class="text-white"><button type="button" class="btn btn-lg btn-primary">Hadicové kolo</button></a>
</div>
@endsection

