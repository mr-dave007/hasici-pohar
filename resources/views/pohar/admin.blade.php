@extends('layouts.app')

@section('title', 'ADMINISTRACE')


@section('content')

<br/>

<div class="tab-content">
@if ($kategorie=='mladsi')
    <div id="mladsiTab" class="tab-pane fade in active show">
@else
    <div id="mladsiTab" class="tab-pane fade in">
@endif

        <table id="tabMladsi" class="std_table table table-striped table-bordered table-hover table-responsive-lg text-nowrap table-condensed">
            <thead class="thead-dark">
            <tr>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Startovní<br/>číslo</th>
                <th class="align-middle font-weight-bold" rowspan="2" scope="col">Družstvo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Hadicové kolo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Dvojice</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">4x60m</th>
                <th class="th-sm text-center font-weight-bold" colspan="3" scope="col">Útok</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Součet</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Pořadí</th>
            </tr>
            <tr>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col" colspan="2">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
            </tr>
            </thead>
            <tbody>
                @foreach($mladsi as $druzstvo)
                    <tr>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['StartCislo'] }}&nbsp;<a class="badge badge-danger" href="/admin/druzstvo/del/{{ $druzstvo['id'] }}">&nbsp;-&nbsp;</a></td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['Druzstvo'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - Hadicové kolo - mladší', {{ $druzstvo['id'] }}, 'Kolo', '{{ $druzstvo['CasKolo'] }}', null);">{{ $druzstvo['CasKolo'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiKolo'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - Dvojice - mladší', {{ $druzstvo['id'] }}, 'Dvojice', '{{ $druzstvo['Cas1Dvojice'] }}', '{{ $druzstvo['Cas2Dvojice'] }}');">{{ $druzstvo['CasDvojice'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiDvojice'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - 4x60m - mladší', {{ $druzstvo['id'] }}, '4x60', '{{ $druzstvo['Cas14x60'] }}', '{{ $druzstvo['Cas24x60'] }}');">{{ $druzstvo['Cas4x60'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['Poradi4x60'] }}</td>
                        
                        <td class="align-middle text-center small pt-1 mt-1 pb-1 mb-1">
                            <a href="#" onClick="ShowModalUtokEdit('{{ $druzstvo['Druzstvo'] }} - Útok - mladší', {{ $druzstvo['id'] }}, '{{ $druzstvo['Cas1Utok'] }}', 1);">{{ $druzstvo['Cas1Utok'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CasUtok'] }}</td>
                        
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiUtok'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CelkovySoucet'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CelkovePoradi'] }}</td>
                    </tr>
                    <tr>
                    <td class="align-middle text-center small pt-1 pb-1 mb-1 mt-1 ">
                        <a href="#" onClick="ShowModalUtokEdit('{{ $druzstvo['Druzstvo'] }} - Útok - mladší', {{ $druzstvo['id'] }}, '{{ $druzstvo['Cas2Utok'] }}', 2);">{{ $druzstvo['Cas2Utok'] }}</a>
                    </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot class="thead-dark">
                <tr>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-green btn-sm m-0 waves-effect" onclick="ShowModalDruzstvoAdd('Přidat družstvo - mladší', 'mladsi');">+</button>
                        <button type="button" class="btn btn-red btn-sm m-0 waves-effect" onclick="window.location.href='/admin/delall/mladsi'">smazat kategorii</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Kolo/mladsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Dvojice/mladsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/4x60/mladsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="3" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Utok/mladsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/results/mladsi/update'">pořadí</button>
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/export'">Excel</button>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>

@if ($kategorie=='starsi')
    <div id="starsiTab" class="tab-pane fade active show"> 
@else
    <div id="starsiTab" class="tab-pane fade"> 
@endif
    <table id="tabMladsi" class="table table-striped table-bordered table-hover table-responsive-lg text-nowrap">
            <thead class="thead-dark">
            <tr>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Startovní<br/>číslo</th>
                <th class="align-middle font-weight-bold" rowspan="2" scope="col">Družstvo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Hadicové kolo</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">Dvojice</th>
                <th class="th-sm text-center font-weight-bold" colspan="2" scope="col">4x60m</th>
                <th class="th-sm text-center font-weight-bold" colspan="3" scope="col">Útok</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Součet</th>
                <th class="th-sm text-center align-middle font-weight-bold" rowspan="2" scope="col">Pořadí</th>
            </tr>
            <tr>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
                <th class="th-sm text-center" scope="col" colspan="2">čas</th>
                <th class="th-sm text-center" scope="col">pořadí</th>
            </tr>
            </thead>
        <tbody>
                @foreach($starsi as $druzstvo)
                    <tr>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['StartCislo'] }}&nbsp;<a class="badge badge-danger" href="/admin/druzstvo/del/{{ $druzstvo['id'] }}">&nbsp;-&nbsp;</a></td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['Druzstvo'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - Hadicové kolo - starší', {{ $druzstvo['id'] }}, 'Kolo', '{{ $druzstvo['CasKolo'] }}', null);">{{ $druzstvo['CasKolo'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiKolo'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - Dvojice - starší', {{ $druzstvo['id'] }}, 'Dvojice', '{{ $druzstvo['Cas1Dvojice'] }}', '{{ $druzstvo['Cas2Dvojice'] }}');">{{ $druzstvo['CasDvojice'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiDvojice'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">
                            <a href="#" onClick="ShowModalDruzstvoEdit('{{ $druzstvo['Druzstvo'] }} - 4x60m - starší', {{ $druzstvo['id'] }}, '4x60', '{{ $druzstvo['Cas14x60'] }}', '{{ $druzstvo['Cas24x60'] }}');">{{ $druzstvo['Cas4x60'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['Poradi4x60'] }}</td>
                        
                        <td class="align-middle text-center small pt-1 mt-1 pb-1 mb-1">
                            <a href="#" onClick="ShowModalUtokEdit('{{ $druzstvo['Druzstvo'] }} - Útok - starší', {{ $druzstvo['id'] }}, '{{ $druzstvo['Cas1Utok'] }}', 1);">{{ $druzstvo['Cas1Utok'] }}</a>
                        </td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CasUtok'] }}</td>
                        
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['PoradiUtok'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CelkovySoucet'] }}</td>
                        <td class="align-middle text-center font-weight-bold" rowspan="2">{{ $druzstvo['CelkovePoradi'] }}</td>
                    </tr>
                    <tr>
                        <td class="align-middle text-center small pt-1 pb-1 mb-1 mt-1 ">
                            <a href="#" onClick="ShowModalUtokEdit('{{ $druzstvo['Druzstvo'] }} - Útok - starší', {{ $druzstvo['id'] }}, '{{ $druzstvo['Cas2Utok'] }}', 2);">{{ $druzstvo['Cas2Utok'] }}</a>
                        </td>
                    </tr>
                @endforeach
        </tbody>
        <tfoot class="thead-dark">
                <tr>
                    <th class="th-sm " colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-green btn-sm m-0 waves-effect" onclick="ShowModalDruzstvoAdd('Přidat družstvo - starší', 'starsi');">+</button>
                        <button type="button" class="btn btn-red btn-sm m-0 waves-effect" onclick="window.location.href='/admin/delall/starsi'">smazat kategorii</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Kolo/starsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Dvojice/starsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/4x60/starsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="3" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/discipline/Utok/starsi/update'">nastav pořadí</button>
                    </th>
                    <th class="th-sm text-center align-middle" colspan="2" scope="col">
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/results/starsi/update'">pořadí</button>
                        <button type="button" class="btn btn-outline-white btn-sm m-0 waves-effect" onclick="window.location.href='/admin/export'">Excel</button>
                    </th>
                </tr>
            </tfoot>
        </table>
    </div>
</div>

<div align="center"><button type="button" class="btn btn-outline-black btn-sm m-0 waves-effect" onclick="window.location.href='/admin/start-listina/export'">Startovní listina</button></div>

<!-- MODAL OKNA -->
<!-- MODAL OKNO - PRIDANI DRUZSTVA -->
<div class="modal fade" id="ModalDruzstvoAdd" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form  method="POST" action="/admin/druzstvo/add">
            {{ csrf_field() }}
            <input type="hidden" id="kategorie" name="kategorie" value="0" />
            <div class="modal-header">
                <h5 class="modal-title text-center font-weight-bold" id="ModalTitle">Chlumec nad Cidlinou - starší</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Název: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="název" id="druzstvo" name="druzstvo" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2">Startovní číslo: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="xx" pattern="[0-9]{1,2}" id="startcislo" name="startcislo" required>
                        </div>
                    </div>
                
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Přidat</button>
            </div>
        </form>
        </div>
    </div>
</div>

<!-- MODAL OKNO - UPRAVA VYSLEDKU DRUZSTVA -->
<div class="modal fade" id="ModalDruzstvoEdit" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form  method="POST" action="/admin/druzstvo/edit">
            {{ csrf_field() }}
            <input type="hidden" id="druzstvoID" name="druzstvoID" value="0" />
            <input type="hidden" id="discipline" name="discipline" value="0" />
            <div class="modal-header">
                <h5 class="modal-title text-center font-weight-bold" id="ModalEditTitle">TITLE</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Čas1: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="xx:xx" id="cas1" name="cas1" required pattern="[0-9]{2,4}:[0-9]{2}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="neplatny1" name="neplatny1" onClick="if (this.checked) { $('#cas1').val('N'); $('#cas1').prop('disabled', true); } else { $('#cas1').val('');$('#cas1').prop('disabled', false); }">
                            <label class="custom-control-label" for="neplatny1">Neplatný čas 1</label>
                        </div>
                    </div>
                </div>
                <div class="row" id="cas2Area">
                <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">Čas2: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="xx:xx" id="cas2" name="cas2" required pattern="[0-9]{2,4}:[0-9]{2}">
                        </div>
                    </div>
                    <div class="col">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="neplatny2" name="neplatny2" onClick="if (this.checked) { $('#cas2').val('N'); $('#cas2').prop('disabled', true); } else { $('#cas2').val('');$('#cas2').prop('disabled', false); }">
                            <label class="custom-control-label" for="neplatny2">Neplatný čas 2</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Uložit čas</button>
            </div>
        </form>
        </div>
    </div>
</div>

<!-- MODAL OKNO - UPRAVA VYSLEDKU DRUZSTVA - UTOK -->
<div class="modal fade" id="UtokModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form  method="POST" action="/admin/druzstvo/edit">
            {{ csrf_field() }}
            <input type="hidden" id="UtokdruzstvoID" name="druzstvoID" value="0" />
            <input type="hidden" id="Utokdiscipline" name="discipline" value="Utok" />
            <input type="hidden" id="Utokpokus" name="pokus" value="1" />
            <div class="modal-header">
                <h5 class="modal-title text-center font-weight-bold" id="ModalUtokEditTitle">Chlumec nad Cidlinou - starší</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon2">čas: </span>
                            </div>
                            <input type="text" class="form-control" placeholder="xx:xx" pattern="[0-9]{2,4}:[0-9]{2}" id="cas" name="cas" required aria-label="Username" aria-describedby="basic-addon2">
                        </div>
                    </div>
                <div class="col">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="neplatny" name="neplatny"  onClick="if (this.checked) { $('#cas').val('N'); $('#cas').prop('disabled', true); } else { $('#cas').val('');$('#cas').prop('disabled', false); }">
                        <label class="custom-control-label" for="neplatny">Neplatný čas</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Uložit čas</button>
        </div>
        </form>
    </div>
</div>
@endsection
