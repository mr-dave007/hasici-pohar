<!DOCTYPE html>
<html>

<head>
    @include('includes.head')
</head>

<body>

@include('includes.navigation')

<div class="container">
        
            @yield('content')
        
</div>

@include('includes.scripts')

</body>
</html>

