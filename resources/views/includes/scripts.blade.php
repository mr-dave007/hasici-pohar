<!-- SCRIPTS -->
  <!-- JQuery -->
  <script type="text/javascript" src="{{ secure_asset('js/jquery-3.3.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="{{ secure_asset('js/popper.min.js') }}"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{ secure_asset('js/bootstrap.min.js') }}"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="{{ secure_asset('js/mdb.min.js') }}"></script>

  <script type="text/javascript">
    $(document).ready(function () {
      @if (preg_match( '/vysledky/', Request::path() ) )
        item = 'starsiTab'
        setInterval(function(){ 
          $('.nav-item a[href="#'+ item +'"]').tab('show'); 
          console.log('Casak'); 
          if (item=='starsiTab') {
            item = 'mladsiTab';
          } else {
            location.reload();
            item = 'starsiTab';
          }
        }, 30000);
      @endif
    });
    function ShowModal(title, idDruzstvo) {
      $('#ModalTitle').html(title);
      $('#druzstvoID').val(idDruzstvo);
      $('#basicModal').modal('show');
    }
    function ShowModalUtok(title, idDruzstvo, pokus) {
      $('#ModalTitle').html(title);
      $('#druzstvoID').val(idDruzstvo);
      $('#pokus').val(pokus);
      $('#UtokModal').modal('show');
    }
    function ShowModalDruzstvoAdd(title, kategorie) {
      $('#ModalTitle').html(title);
      $('#kategorie').val(kategorie);
      $('#ModalDruzstvoAdd').modal('show');
    }
    function ShowModalUtokEdit(title, idDruzstvo, cas, pokus) {
      $('#ModalUtokEditTitle').html(title);
      $('#UtokdruzstvoID').val(idDruzstvo);
      $('#Utokpokus').val(pokus);
      if (cas=='N') {
        $('#cas').val('N');
        $('#cas').prop('disabled', true);
        $('#neplatny').prop('checked', true);
      } else {
        $('#cas').val(cas);
        $('#cas').prop('disabled', false);
        $('#neplatny').prop('checked', false);
      }
      $('#UtokModal').modal('show');
    }
    function ShowModalDruzstvoEdit(title, id, discipline, cas1, cas2) {
      $('#ModalEditTitle').html(title);
      $('#druzstvoID').val(id);
      $('#discipline').val(discipline);
      if (cas1=='N') {
        $('#cas1').val('N');
        $('#cas1').prop('disabled', true);
        $('#neplatny1').prop('checked', true);
      } else {
        $('#cas1').val(cas1);
        $('#cas1').prop('disabled', false);
        $('#neplatny1').prop('checked', false);
      }

      if (cas2==null) {
        $('#cas2Area').addClass('d-none');
        $('#cas2').prop('disabled', true);
      } else {
        $('#cas2Area').removeClass('d-none');
        $('#cas2').prop('disabled', false);
      }

      if (cas2=='N') {
        $('#cas2').val('N');
        $('#cas2').prop('disabled', true);
        $('#neplatny2').prop('checked', true);
      } else {
        $('#cas2').val(cas2);
        $('#cas2').prop('disabled', false);
        $('#neplatny2').prop('checked', false);
      }

      
      $('#ModalDruzstvoEdit').modal('show');
    }
    
  </script>