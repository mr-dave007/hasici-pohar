<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Hasiči POHÁR 2023 - @yield('title')</title>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ secure_asset('css/fontawesome/all.css') }}">
  <!-- Bootstrap core CSS -->
  <link href="{{ secure_asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" >

  <!-- Material Design Bootstrap -->
  <link href="{{ secure_asset('css/mdb.min.css') }}" rel="stylesheet">
  <!-- Your custom styles (optional) -->
  <link href="{{ secure_asset('css/style.css') }}" rel="stylesheet">

