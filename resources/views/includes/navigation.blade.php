<h2 class="text-center font-weight-bold z-depth-5 page-header py-2">Chlumecký pohár {{ now()->year }}</h2>
@isset ($kategorie)
<ul class="nav nav-pills nav-fill font-weight-bold">
    <li class="nav-item"><div class="nav-link disabled">@yield('title')</div></li>
    @if ($kategorie=='mladsi')
    <li class="nav-item"><a data-toggle="pill" class="nav-link active" href="#mladsiTab">MLADŠÍ</a></li>
    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#starsiTab">STARŠÍ</a></li>
    @else
    <li class="nav-item"><a data-toggle="pill" class="nav-link" href="#mladsiTab">MLADŠÍ</a></li>
    <li class="nav-item"><a data-toggle="pill" class="nav-link active" href="#starsiTab">STARŠÍ</a></li>
    @endif
</ul>
@endisset
