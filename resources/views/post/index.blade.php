@extends('layouts.app')

@section('title', 'Příspěvky')

@section('content')

    @foreach($posts as $post)
        <div class="card mb-4">
            <div class="card-body">
                <h2 class="card-title">{{ $post->StartCislo }}</h2>
                <p class="card-text">{{ $post->Druzstvo }}</p>
            </div>
        </div>
    @endforeach
 @endsection

