<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

define('NEPLATNY', 1000000);

class HasiciPohar extends Model
{
    use HasFactory;
    static public function _4x60_Get($kategorie = 'mladsi') {
        $druzstva = array();
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('StartCislo')->get() as $item) {
            $platnyCas = false;
            $Cas1 = $Cas2 = $VyslednyCas= null;
            $casPokus = new \DateTime($item->{'4x60_UkonceniCas'}, new \DateTimeZone('Europe/Prague'));
            $casNyni = new \DateTime('now', new \DateTimeZone('Europe/Prague'));
            if (($casNyni->diff($casPokus)->format('%i'))>15) $platnyCas = true;
            if ($item->{'4x60_Cas1'}!=null && $item->{'4x60_Cas1'}!=NEPLATNY) {
                $Cas1 = (int)($item->{'4x60_Cas1'}/100).":".sprintf("%02d", $item->{'4x60_Cas1'} % 100);
            }
            if ($item->{'4x60_Cas1'}==NEPLATNY) {
                $Cas1 = 'N';
            }
            if ($item->{'4x60_Cas2'}!=null && $item->{'4x60_Cas2'}!=NEPLATNY) {
                $Cas2 = (int)($item->{'4x60_Cas2'}/100).":".sprintf("%02d", $item->{'4x60_Cas2'} % 100);
            }
            if ($item->{'4x60_Cas2'}==NEPLATNY) {
                $Cas2 = 'N';
            }
            
            if ($item->{'4x60_VyslednyCas'}!=null && $item->{'4x60_VyslednyCas'}!=NEPLATNY) {
                $VyslednyCas = (int)($item->{'4x60_VyslednyCas'}/100).":".sprintf("%02d", $item->{'4x60_VyslednyCas'} % 100);
            }
            if ($item->{'4x60_VyslednyCas'}==NEPLATNY) {
                $VyslednyCas = 'N';
            }
            $druzstva[] = [
                'id' => $item->id,
                'StartCislo' => $item->StartCislo,
                'Druzstvo' => $item->Druzstvo,
                'Cas1' => $Cas1,
                'Cas2' => $Cas2,
                'Platny' => $platnyCas,
                'VyslednyCas' => $VyslednyCas,
                'Poradi' => $item->{'4x60_Poradi'},
            ];
        }
        return $druzstva;
    }
    static public function _4x60_Store($ReqId, $ReqCas1, $ReqCas2, $ReqNeplatny1, $ReqNeplatny2) {
        $Cas1 = $Cas2 = NEPLATNY;
        if (!$ReqNeplatny1 || $ReqNeplatny1=='off' ) {
            $CasExpl = explode(':', $ReqCas1);
            $Cas1 = ($CasExpl[0]*100) + $CasExpl[1];
        }
        if (!$ReqNeplatny2 || $ReqNeplatny2=='off') {
            $CasExpl = explode(':', $ReqCas2);
            $Cas2 = ($CasExpl[0]*100) + $CasExpl[1];
        }
        $VyslednyCas = $Cas1;
        if ($Cas2<$Cas1) $VyslednyCas = $Cas2;
        HasiciPohar::where(['id'=>$ReqId])->update(['4x60_Cas1'=>$Cas1, '4x60_Cas2'=>$Cas2,'4x60_VyslednyCas'=>$VyslednyCas, '4x60_UkonceniCas'=>\DB::raw('now()')]);
        return true;
    }
    static public function _Dvojice_Get($kategorie = 'mladsi') {
        $druzstva = array();
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('StartCislo')->get() as $item) {
            $platnyCas = false;
            $Cas1 = $Cas2 = $VyslednyCas= null;
            $casPokus = new \DateTime($item->{'Dvojice_UkonceniCas'}, new \DateTimeZone('Europe/Prague'));
            $casNyni = new \DateTime('now', new \DateTimeZone('Europe/Prague'));
            if (($casNyni->diff($casPokus)->format('%i'))>15) $platnyCas = true;
            if ($item->{'Dvojice_Cas1'}!=null && $item->{'Dvojice_Cas1'}!=NEPLATNY) {
                $Cas1 = (int)($item->{'Dvojice_Cas1'}/100).":".sprintf("%02d", $item->{'Dvojice_Cas1'} % 100);
            }
            if ($item->{'Dvojice_Cas1'}==NEPLATNY) {
                $Cas1 = 'N';
            }
            if ($item->{'Dvojice_Cas2'}!=null && $item->{'Dvojice_Cas2'}!=NEPLATNY) {
                $Cas2 = (int)($item->{'Dvojice_Cas2'}/100).":".sprintf("%02d", $item->{'Dvojice_Cas2'} % 100);
            }
            if ($item->{'Dvojice_Cas2'}==NEPLATNY) {
                $Cas2 = 'N';
            }
            if ($item->{'Dvojice_VyslednyCas'}!=null && $item->{'Dvojice_VyslednyCas'}!=NEPLATNY) {
                $VyslednyCas = (int)($item->{'Dvojice_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Dvojice_VyslednyCas'} % 100);
            }
            if ($item->{'Dvojice_VyslednyCas'}==NEPLATNY) {
                $VyslednyCas = 'N';
            }
            $druzstva[] = [
                'id' => $item->id,
                'StartCislo' => $item->StartCislo,
                'Druzstvo' => $item->Druzstvo,
                'Cas1' => $Cas1,
                'Cas2' => $Cas2,
                'Platny' => $platnyCas,
                'VyslednyCas' => $VyslednyCas,
                'Poradi' => $item->{'Dvojice_Poradi'},
            ];
        }
        return $druzstva;
    }
    static public function _Dvojice_Store($ReqId, $ReqCas1, $ReqCas2, $ReqNeplatny1, $ReqNeplatny2) {
        $Cas1 = $Cas2 = NEPLATNY;
        if (!$ReqNeplatny1 || $ReqNeplatny1=='off' ) {
            $CasExpl = explode(':', $ReqCas1);
            $Cas1 = ($CasExpl[0]*100) + $CasExpl[1];
        }
        if (!$ReqNeplatny2 || $ReqNeplatny2=='off') {
            $CasExpl = explode(':', $ReqCas2);
            $Cas2 = ($CasExpl[0]*100) + $CasExpl[1];
        }
        $VyslednyCas = $Cas1;
        if ($Cas2<$Cas1) $VyslednyCas = $Cas2;
        HasiciPohar::where(['id'=>$ReqId])->update(['Dvojice_Cas1'=>$Cas1, 'Dvojice_Cas2'=>$Cas2,'Dvojice_VyslednyCas'=>$VyslednyCas, 'Dvojice_UkonceniCas'=>\DB::raw('now()')]);
        return true;
    }
    static public function _Utok_Get($kategorie = 'mladsi') {
        $druzstva = array();
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('StartCislo')->get() as $item) {
            $platnyCas1 = $platnyCas2 = false;
            $Cas1 = $Cas2 = $VyslednyCas= null;

            $casPokus1 = new \DateTime($item->{'Utok_UkonceniCas1'}, new \DateTimeZone('Europe/Prague'));
            $casPokus2 = new \DateTime($item->{'Utok_UkonceniCas2'}, new \DateTimeZone('Europe/Prague'));
            $casNyni = new \DateTime('now', new \DateTimeZone('Europe/Prague'));
            if (($casNyni->diff($casPokus1)->format('%i'))>15) $platnyCas1 = true;
            if (($casNyni->diff($casPokus2)->format('%i'))>15) $platnyCas2 = true;

            if ($item->{'Utok_Cas1'}!=null && $item->{'Utok_Cas1'}!=NEPLATNY) {
                $Cas1 = (int)($item->{'Utok_Cas1'}/100).":".sprintf("%02d", $item->{'Utok_Cas1'} % 100);
            }
            if ($item->{'Utok_Cas1'}==NEPLATNY) {
                $Cas1 = 'N';
            }
            if ($item->{'Utok_Cas2'}!=null && $item->{'Utok_Cas2'}!=NEPLATNY) {
                $Cas2 = (int)($item->{'Utok_Cas2'}/100).":".sprintf("%02d", $item->{'Utok_Cas2'} % 100);
            }
            if ($item->{'Utok_Cas2'}==NEPLATNY) {
                $Cas2 = 'N';
            }
            if ($item->{'Utok_VyslednyCas'}!=null && $item->{'Utok_VyslednyCas'}!=NEPLATNY) {
                $VyslednyCas = (int)($item->{'Utok_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Utok_VyslednyCas'} % 100);
            }
            if ($item->{'Utok_VyslednyCas'}==NEPLATNY) {
                $VyslednyCas = 'N';
            }
            $druzstva[] = [
                'id' => $item->id,
                'StartCislo' => $item->StartCislo,
                'Druzstvo' => $item->Druzstvo,
                'Cas1' => $Cas1,
                'Cas2' => $Cas2,
                'Platny1' => $platnyCas1,
                'Platny2' => $platnyCas2,
                'VyslednyCas' => $VyslednyCas,
                'Poradi' => $item->{'Utok_Poradi'},
            ];
        }


        return $druzstva;
    }
    static public function _Utok_Store($ReqId, $ReqPokus, $ReqCas, $ReqNeplatny) {
        $Cas = NEPLATNY;
        if (!$ReqNeplatny || $ReqNeplatny=='off' ) {
            $CasExpl = explode(':', $ReqCas);
            $Cas = ($CasExpl[0]*100) + $CasExpl[1];
        }
        if ($ReqPokus == 1) { // Prvni pokus
            HasiciPohar::where(['id'=>$ReqId])->update(['Utok_Cas1'=>$Cas, 'Utok_UkonceniCas1'=>\DB::raw('now()')]);
        } else { // druhy pokus
            $Druzstvo = HasiciPohar::find($ReqId);
            if (($Druzstvo->Utok_Cas1)>$Cas) {
                HasiciPohar::where(['id'=>$ReqId])->update(['Utok_Cas2'=>$Cas, 'Utok_UkonceniCas2'=>\DB::raw('now()'), 'Utok_VyslednyCas'=>$Cas]);
            } else {
                HasiciPohar::where(['id'=>$ReqId])->update(['Utok_Cas2'=>$Cas, 'Utok_UkonceniCas2'=>\DB::raw('now()'), 'Utok_VyslednyCas'=>$Druzstvo->Utok_Cas1]);
            }
        }
        return true;
    }
    static public function _Utok_AdminUpdate($ReqId, $ReqCas1, $ReqNeplatny1, $ReqCas2, $ReqNeplatny2) {
        /*
        $Cas1 = NEPLATNY;
        if (!$ReqNeplatny1 || $ReqNeplatny1=='off' ) {
            $CasExpl = explode(':', $ReqCas);
            $Cas1 = ($CasExpl[0]*100) + $CasExpl[1];
        }
        */
        return true;
    }
    static public function _Kolo_Get($kategorie = 'mladsi') {
        $druzstva = array();
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('StartCislo')->get() as $item) {
            $platnyCas = false;
            $VyslednyCas= null;

            $casPokus = new \DateTime($item->{'Kolo_UkonceniCas'}, new \DateTimeZone('Europe/Prague'));
            $casNyni = new \DateTime('now', new \DateTimeZone('Europe/Prague'));
            if (($casNyni->diff($casPokus)->format('%i'))>15) $platnyCas = true;
            if ($item->{'Kolo_VyslednyCas'}!=null && $item->{'Kolo_VyslednyCas'}!=NEPLATNY) {
                $VyslednyCas = (int)($item->{'Kolo_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Kolo_VyslednyCas'} % 100);
            }
            if ($item->{'Kolo_VyslednyCas'}==NEPLATNY) {
                $VyslednyCas = 'N';
            }
            $druzstva[] = [
                'id' => $item->id,
                'StartCislo' => $item->StartCislo,
                'Druzstvo' => $item->Druzstvo,
                'Platny' => $platnyCas,
                'VyslednyCas' => $VyslednyCas,
                'Poradi' => $item->{'Kolo_Poradi'},
            ];
        }
        return $druzstva;
    }
    static public function _Kolo_Store($ReqId, $ReqCas, $ReqNeplatny) {
        $Cas = NEPLATNY;
        if (!$ReqNeplatny || $ReqNeplatny=='off' ) {
            $CasExpl = explode(':', $ReqCas);
            $Cas = ($CasExpl[0]*100) + $CasExpl[1];
        }
        HasiciPohar::where(['id'=>$ReqId])->update(['Kolo_VyslednyCas'=>$Cas, 'Kolo_UkonceniCas'=>\DB::raw('now()')]);
        return true;
    }
    static public function _Results_Get($kategorie = 'mladsi') {
        $druzstva = array();
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('StartCislo')->get() as $item) {
            $CasKolo = $CasDvojice = $Cas1Dvojice = $Cas2Dvojice = $Cas4x60 = $Cas14x60 = $Cas24x60 = $Cas1Utok = $Cas2Utok = $CasUtok = null;
            if ($item->{'Kolo_VyslednyCas'}!=null && $item->{'Kolo_VyslednyCas'}!=NEPLATNY) {
                $CasKolo = (int)($item->{'Kolo_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Kolo_VyslednyCas'} % 100);
            }
            if ($item->{'Kolo_VyslednyCas'}==NEPLATNY) {
                $CasKolo = 'N';
            }
            if ($item->{'Dvojice_Cas1'}!=null && $item->{'Dvojice_Cas1'}!=NEPLATNY) {
                $Cas1Dvojice = (int)($item->{'Dvojice_Cas1'}/100).":".sprintf("%02d", $item->{'Dvojice_Cas1'} % 100);
            }
            if ($item->{'Dvojice_Cas1'}==NEPLATNY) {
                $Cas1Dvojice = 'N';
            }
            if ($item->{'Dvojice_Cas2'}!=null && $item->{'Dvojice_Cas2'}!=NEPLATNY) {
                $Cas2Dvojice = (int)($item->{'Dvojice_Cas2'}/100).":".sprintf("%02d", $item->{'Dvojice_Cas2'} % 100);
            }
            if ($item->{'Dvojice_Cas2'}==NEPLATNY) {
                $Cas2Dvojice = 'N';
            }
            if ($item->{'Dvojice_VyslednyCas'}!=null && $item->{'Dvojice_VyslednyCas'}!=NEPLATNY) {
                $CasDvojice = (int)($item->{'Dvojice_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Dvojice_VyslednyCas'} % 100);
            }
            if ($item->{'Dvojice_VyslednyCas'}==NEPLATNY) {
                $CasDvojice = 'N';
            }
            if ($item->{'4x60_VyslednyCas'}!=null && $item->{'4x60_VyslednyCas'}!=NEPLATNY) {
                $Cas4x60 = (int)($item->{'4x60_VyslednyCas'}/100).":".sprintf("%02d", $item->{'4x60_VyslednyCas'} % 100);
            }
            if ($item->{'4x60_VyslednyCas'}==NEPLATNY) {
                $Cas4x60 = 'N';
            }

            if ($item->{'4x60_Cas1'}!=null && $item->{'4x60_Cas1'}!=NEPLATNY) {
                $Cas14x60 = (int)($item->{'4x60_Cas1'}/100).":".sprintf("%02d", $item->{'4x60_Cas1'} % 100);
            }
            if ($item->{'4x60_Cas1'}==NEPLATNY) {
                $Cas14x60 = 'N';
            }
            if ($item->{'4x60_Cas2'}!=null && $item->{'4x60_Cas2'}!=NEPLATNY) {
                $Cas24x60 = (int)($item->{'4x60_Cas2'}/100).":".sprintf("%02d", $item->{'4x60_Cas2'} % 100);
            }
            if ($item->{'4x60_Cas2'}==NEPLATNY) {
                $Cas24x60 = 'N';
            }

            if ($item->{'Utok_Cas1'}!=null && $item->{'Utok_Cas1'}!=NEPLATNY) {
                $Cas1Utok = (int)($item->{'Utok_Cas1'}/100).":".sprintf("%02d", $item->{'Utok_Cas1'} % 100);
            }
            if ($item->{'Utok_Cas1'}==NEPLATNY) {
                $Cas1Utok = 'N';
            }
            if ($item->{'Utok_Cas2'}!=null && $item->{'Utok_Cas2'}!=NEPLATNY) {
                $Cas2Utok = (int)($item->{'Utok_Cas2'}/100).":".sprintf("%02d", $item->{'Utok_Cas2'} % 100);
            }
            if ($item->{'Utok_Cas2'}==NEPLATNY) {
                $Cas2Utok = 'N';
            }

            if ($item->{'Utok_VyslednyCas'}!=null && $item->{'Utok_VyslednyCas'}!=NEPLATNY) {
                $CasUtok = (int)($item->{'Utok_VyslednyCas'}/100).":".sprintf("%02d", $item->{'Utok_VyslednyCas'} % 100);
            }
            if ($item->{'Utok_VyslednyCas'}==NEPLATNY) {
                $CasUtok = 'N';
            }
            $druzstva[] = [
                'id' => $item->id,
                'StartCislo' => $item->StartCislo,
                'Druzstvo' => $item->Druzstvo,
                'CasKolo' => $CasKolo,
                'PoradiKolo' => $item->{'Kolo_Poradi'},
                'CasDvojice' => $CasDvojice,
                'Cas1Dvojice' => $Cas1Dvojice,
                'Cas2Dvojice' => $Cas2Dvojice,
                'PoradiDvojice' => $item->{'Dvojice_Poradi'},
                'Cas24x60' => $Cas14x60,
                'Cas14x60' => $Cas24x60,
                'Cas4x60' => $Cas4x60,
                'Poradi4x60' => $item->{'4x60_Poradi'},
                'Cas1Utok' => $Cas1Utok,
                'Cas2Utok' => $Cas2Utok,
                'CasUtok' => $CasUtok,
                'PoradiUtok' => $item->{'Utok_Poradi'},
                'CelkovySoucet' => $item->CelkovySoucet,
                'CelkovePoradi' => $item->CelkovePoradi,
            ];
        }
        return $druzstva;
    }
    static public function _SpocitejPoradi_($disciplina, $kategorie) {
        $lastResult = 0;
        $poradi = 0;
        $poradiVynecham = 1;
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy($disciplina.'_VyslednyCas')->get() as $item) {
            if ($lastResult!=$item->{$disciplina.'_VyslednyCas'}) {
                if ($poradiVynecham>1) {
                    $poradi = $poradi + $poradiVynecham;
                    $poradiVynecham = 1;
                }
                else {
                    $poradi++;
                }
            } else {
                $poradiVynecham++;
            }
            $lastResult = $item->{$disciplina.'_VyslednyCas'};
            HasiciPohar::where(['id'=>$item->id])->update([$disciplina.'_Poradi'=>$poradi]);
        }
        return true;
    }
    static public function _SpocitejCelkovePoradi_($kategorie) {
        $soucetPoradi = 0;
        foreach (HasiciPohar::where('kategorie', $kategorie)->get() as $item) {
            $soucetPoradi = $item->{'4x60_Poradi'} + $item->{'Dvojice_Poradi'} + $item->{'Utok_Poradi'};
            HasiciPohar::where(['id'=>$item->id])->update(['CelkovySoucet'=>$soucetPoradi]);
        }
        // PROVEDU VYPOCET CELKOVEHO PORADI V DANE KATEGORII
        $poradi = 1;
        foreach (HasiciPohar::where('kategorie', $kategorie)->orderBy('CelkovySoucet')->orderBy('Utok_Poradi')->orderBy('4x60_Poradi')->orderBy('Dvojice_Poradi')->get() as $item) {
            HasiciPohar::where(['id'=>$item->id])->update(['CelkovePoradi'=>$poradi]);
            $poradi++;
        }
        return true;
    }
}
