<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\HasiciPohar;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Pohar extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pohar.index');
        //
    }
    public function vysledky()
    {
        return view('pohar.vysledky', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => 'mladsi']);
        //
    }

    /*
    *   ADMINISTRACE
    */
    public function indexAdmin() { // zobrazeni celkovych vysledku
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => 'mladsi']);
    }
    public function AdminDruzstvoAdd(Request $request) { // pridani druzstva
        $druzstvo = new HasiciPohar;
        $druzstvo->Druzstvo = $request->druzstvo;
        $druzstvo->StartCislo = $request->startcislo;
        $druzstvo->kategorie = $request->kategorie;
        $druzstvo->save();

        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $request->kategorie]);
    }
    public function AdminDruzstvoDel($id) { // smaze druzstvo
        $druzstvo = HasiciPohar::find($id);
        $kategorie = $druzstvo->kategorie;
        HasiciPohar::where('id', $id)->delete();
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $kategorie]);
    }
    public function AdminDruzstvoDelAll($kategorie) { // smaze vsechna druzstva v kategorii
        HasiciPohar::where('kategorie', $kategorie)->delete();
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $kategorie]);
    } 
    public function AdminDruzstvoUpdate(Request $request) { // upravi vysledne casy druzstva
        switch ($request->discipline) {
            case 'Utok':
                HasiciPohar::_Utok_Store($request->druzstvoID, $request->pokus, $request->cas, $request->neplatny);
                $druzstvo = HasiciPohar::find($request->druzstvoID);
                if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Utok_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Utok', $druzstvo->kategorie);
                }
            break;
            case '4x60':
                HasiciPohar::_4x60_Store($request->druzstvoID, $request->cas1, $request->cas2, $request->neplatny1, $request->neplatny2);
                $druzstvo = HasiciPohar::find($request->druzstvoID);
                if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('4x60_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('4x60', $druzstvo->kategorie);
                }
            break;
            case 'Dvojice':
                HasiciPohar::_Dvojice_Store($request->druzstvoID, $request->cas1, $request->cas2, $request->neplatny1, $request->neplatny2);
                $druzstvo = HasiciPohar::find($request->druzstvoID);
                if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Dvojice_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Dvojice', $druzstvo->kategorie);
                }
            break;
            case 'Kolo':
                HasiciPohar::_Kolo_Store($request->druzstvoID, $request->cas1, $request->neplatny1);
                $druzstvo = HasiciPohar::find($request->druzstvoID);
                if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Kolo_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Kolo', $druzstvo->kategorie);
                }
            break;

        }
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $druzstvo->kategorie]);
    }
    public function AdminDisciplineUpdate($discipline, $kategorie) { // vypocita poradi discipliny
        switch ($discipline) {
        case 'Utok':
                if (HasiciPohar::where('kategorie', $kategorie)->wherenull('Utok_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Utok', $kategorie);
                }
            break;
            case '4x60':
                if (HasiciPohar::where('kategorie', $kategorie)->wherenull('4x60_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('4x60', $kategorie);
                }
            break;
            case 'Dvojice':
                if (HasiciPohar::where('kategorie', $kategorie)->wherenull('Dvojice_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Dvojice', $kategorie);
                }
            break;
            case 'Kolo':
                if (HasiciPohar::where('kategorie', $kategorie)->wherenull('Kolo_VyslednyCas')->count()==0) {
                    HasiciPohar::_SpocitejPoradi_('Kolo', $kategorie);
                }
            break;
        }
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $kategorie]);
    }
    public function AdminResultsUpdate($kategorie) { // vypocita celkove poradi 
        if (HasiciPohar::where('kategorie', $kategorie)->wherenull('Dvojice_Poradi')->wherenull('4x60_Poradi')->wherenull('Utok_Poradi')->count()==0) {
            HasiciPohar::_SpocitejCelkovePoradi_($kategorie);
        }
        return view('pohar.admin', ['mladsi' => HasiciPohar::_Results_Get('mladsi'), 'starsi' => HasiciPohar::_Results_Get('starsi'), 'kategorie' => $kategorie]);
    }
    public function XlsTemplate(&$sheet, $title) {
        $idx = 1;
        $sheet->setTitle($title);
        $idx = 1;
        $sheet->setCellValue('A'.$idx, 'Chlumecký pohár '.Date('Y')." - ".$title);
        $sheet->getStyle('A'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('A'.$idx)->getFont()->getColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE);
        $sheet->getStyle('A'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('A'.$idx)->getFill()->getStartColor()->setARGB('FF000000');
        $sheet->getStyle('A'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A'.$idx)->getFont()->setSize(20);
        $sheet->getStyle('A'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	    $sheet->getRowDimension($idx)->setRowHeight(25); 
        $sheet->mergeCells("A{$idx}:L{$idx}");
        $idx++;

        $sheet->setCellValue('A'.$idx, 'Startovní číslo');
        $sheet->getStyle('A'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('A'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('A'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('A'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('A'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("A{$idx}:A".($idx+1));
        $sheet->getRowDimension($idx)->setRowHeight(40);
        
        $sheet->setCellValue('B'.$idx, 'Družstvo');
        $sheet->getStyle('B'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('B'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('B'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('B'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('B'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('B'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('B'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("B{$idx}:B".($idx+1));
	    $sheet->getRowDimension($idx)->setRowHeight(40);

        $sheet->setCellValue('C'.$idx, 'Hadicové kolo');
        $sheet->getStyle('C'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('C'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('C'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('C'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('C'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("C{$idx}:D{$idx}");
        $sheet->getRowDimension($idx)->setRowHeight(20);

        $sheet->setCellValue('E'.$idx, 'Štafeta dvojic');
        $sheet->getStyle('E'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('E'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('E'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('E'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('E'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('E'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("E{$idx}:F{$idx}");
        $sheet->getRowDimension($idx)->setRowHeight(20);

        $sheet->setCellValue('G'.$idx, 'Štafeta 4x60m');
        $sheet->getStyle('G'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('G'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('G'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('G'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('G'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('G'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("G{$idx}:H{$idx}");
        $sheet->getRowDimension($idx)->setRowHeight(20);
        
        $sheet->setCellValue('I'.$idx, 'Požární útok');
        $sheet->getStyle('I'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('I'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('I'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('I'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('I'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('I'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('I'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("I{$idx}:J{$idx}");
        $sheet->getRowDimension($idx)->setRowHeight(20);

        $sheet->setCellValue('K'.$idx, 'Součet');
        $sheet->getStyle('K'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('K'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('K'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('K'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('K'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('K'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('K'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('K'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("K{$idx}:K".($idx+1));
        $sheet->getRowDimension($idx)->setRowHeight(40);
        
        $sheet->setCellValue('L'.$idx, 'Pořadí');
        $sheet->getStyle('L'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('L'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('L'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('L'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('L'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('L'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('L'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('L'.$idx)->getAlignment()->setWrapText(true);
        $sheet->mergeCells("L{$idx}:L".($idx+1));
        $sheet->getRowDimension($idx)->setRowHeight(40);

        $idx++;
        $sheet->setCellValue('C'.$idx, 'čas');
        $sheet->getStyle('C'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('C'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $sheet->getStyle('C'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('C'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('C'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('D'.$idx, 'pořadí');
        $sheet->getStyle('D'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('D'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('D'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('D'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('D'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('D'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('E'.$idx, 'čas');
        $sheet->getStyle('E'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('E'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('E'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('E'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('E'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('E'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('F'.$idx, 'pořadí');
        $sheet->getStyle('F'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('F'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('F'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('F'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('F'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('F'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('F'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('G'.$idx, 'čas');
        $sheet->getStyle('G'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('G'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('G'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('G'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('G'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('G'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('H'.$idx, 'pořadí');
        $sheet->getStyle('H'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('H'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('H'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('H'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('H'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('H'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('H'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('H'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('I'.$idx, 'čas');
        $sheet->getStyle('I'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('I'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('I'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('I'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('I'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('I'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('I'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('I'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        $sheet->setCellValue('J'.$idx, 'pořadí');
        $sheet->getStyle('J'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('J'.$idx)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);;
        $sheet->getStyle('J'.$idx)->getFill()->getStartColor()->setARGB('FFAAAAAA');
        $sheet->getStyle('J'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('J'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('J'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('J'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('J'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);

        return true;
    }
    public function XlsTemplateResults(&$sheet, $results) {
        $idx = 4;
        foreach($results  as $druzstvo) {
	        $sheet->setCellValue('A'.$idx, $druzstvo['StartCislo']);
	        $sheet->getStyle('A'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('A'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('A'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('A'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('B'.$idx, $druzstvo['Druzstvo']);
	        $sheet->getStyle('B'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('B'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('B'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('C'.$idx, $druzstvo['CasKolo']);
	        $sheet->getStyle('C'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('C'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('C'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('C'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('D'.$idx, $druzstvo['PoradiKolo']);
	        $sheet->getStyle('D'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('D'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('D'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('D'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('E'.$idx, $druzstvo['CasDvojice']);
	        $sheet->getStyle('E'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('E'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('E'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('E'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('F'.$idx, $druzstvo['PoradiDvojice']);
	        $sheet->getStyle('F'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('F'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('F'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('F'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('G'.$idx, $druzstvo['Cas4x60']);
	        $sheet->getStyle('G'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('G'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('G'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('G'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('H'.$idx, $druzstvo['Poradi4x60']);
	        $sheet->getStyle('H'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('H'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('H'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('H'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('I'.$idx, $druzstvo['CasUtok']);
	        $sheet->getStyle('I'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('I'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('I'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('I'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('J'.$idx, $druzstvo['PoradiUtok']);
	        $sheet->getStyle('J'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('J'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('J'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('J'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('K'.$idx, $druzstvo['CelkovySoucet']);
	        $sheet->getStyle('K'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('K'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('K'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('K'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
            $sheet->setCellValue('L'.$idx, $druzstvo['CelkovePoradi']);
	        $sheet->getStyle('L'.$idx)->getFont()->setBold(true);
            $sheet->getStyle('L'.$idx)->getFont()->setSize(10);
            $sheet->getStyle('L'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $sheet->getStyle('L'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

	        $sheet->getRowDimension($idx)->setRowHeight(18); 

	        $idx++;
        }
        $sheet->setCellValue('A'.$idx, 'Výsledky hadicového kola se nezapočítávají do celkového pořadí');
        $sheet->getStyle('A'.$idx)->getFont()->setSize(8);

        $sheet->getColumnDimension('A')->setWidth(12);
        $sheet->getColumnDimension('B')->setWidth(20); 
        $sheet->getColumnDimension('C')->setWidth(10);
        $sheet->getColumnDimension('D')->setWidth(10); 
        $sheet->getColumnDimension('E')->setWidth(10); 
        $sheet->getColumnDimension('F')->setWidth(10); 
        $sheet->getColumnDimension('G')->setWidth(10); 
        $sheet->getColumnDimension('H')->setWidth(10); 
        $sheet->getColumnDimension('I')->setWidth(10); 
        $sheet->getColumnDimension('J')->setWidth(10); 
        $sheet->getColumnDimension('K')->setWidth(10); 
        $sheet->getColumnDimension('L')->setWidth(10); 
        return true;
    }
    public function AdminResults2Xlsx() { // vygeneruje XLSX vysledky
        $dokument = new Spreadsheet();
        $dokument->setActiveSheetIndex(0);
        $sheet = $dokument->getActiveSheet();
        $sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $sheet->getPageMargins()->setTop(1);
        $sheet->getPageMargins()->setLeft(0.5);
        $sheet->getPageMargins()->setRight(0.5);
        $sheet->getPageMargins()->setBottom(0.5);

        $this->XlsTemplate($sheet, 'starší');
        $this->XlsTemplateResults($sheet, HasiciPohar::_Results_Get('starsi'));

        $sheetMladsi = $dokument->createSheet();;
        $sheetMladsi->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $sheetMladsi->getPageMargins()->setTop(1);
        $sheetMladsi->getPageMargins()->setLeft(0.5);
        $sheetMladsi->getPageMargins()->setRight(0.5);
        $sheetMladsi->getPageMargins()->setBottom(0.5);
        $this->XlsTemplate($sheetMladsi, 'mladší');
        $this->XlsTemplateResults($sheetMladsi, HasiciPohar::_Results_Get('mladsi'));
        
        $writer = new Xlsx($dokument);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="Vysledky.xlsx"');
        $writer->save("php://output");
        
    }
    public function AdminStartListinaTemplate(&$sheet, $title) {
        $idx = 1;
        $sheet->setTitle($title);
        $sheet->setCellValue('A'.$idx, "Požární útok  - ".$title);
        $sheet->getStyle('A'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('A'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A'.$idx)->getFont()->setSize(20);
        $sheet->getStyle('A'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
	    $sheet->getRowDimension($idx)->setRowHeight(25); 
        $sheet->mergeCells("A{$idx}:G{$idx}");
        $idx++;

        $sheet->setCellValue('A'.$idx, 'Startovní číslo');
        $sheet->getStyle('A'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('A'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('A'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('A'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('A'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('A'.$idx)->getAlignment()->setWrapText(true);
        $sheet->getRowDimension($idx)->setRowHeight(20);
        
        $sheet->setCellValue('B'.$idx, 'Družstvo');
        $sheet->getStyle('B'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('B'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('B'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('B'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('B'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('B'.$idx)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('C'.$idx, 'čas 1');
        $sheet->getStyle('C'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('C'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('C'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('C'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('C'.$idx)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('D'.$idx, 'čas ukončení 1');
        $sheet->getStyle('D'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('D'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('D'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('D'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('D'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('D'.$idx)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('E'.$idx, 'čas 2');
        $sheet->getStyle('E'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('E'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('E'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('E'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('E'.$idx)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('F'.$idx, 'čas ukončení 2');
        $sheet->getStyle('F'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('F'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('F'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('F'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('F'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('F'.$idx)->getAlignment()->setWrapText(true);

        $sheet->setCellValue('G'.$idx, 'výsledný čas');
        $sheet->getStyle('G'.$idx)->getFont()->setBold(true);
        $sheet->getStyle('G'.$idx)->getFont()->setSize(14);
        $sheet->getStyle('G'.$idx)->getBorders()->getAllBorders()->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
        $sheet->getStyle('G'.$idx)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
        $sheet->getStyle('G'.$idx)->getAlignment()->setWrapText(true);

        return true;
    }
    public function AdminStartListina2Xlsx() {
        $dokument = new Spreadsheet();
        $dokument->setActiveSheetIndex(0);
        $sheetStarsi = $dokument->getActiveSheet();
        $sheetStarsi->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $sheetStarsi->getPageMargins()->setTop(1);
        $sheetStarsi->getPageMargins()->setLeft(0.5);
        $sheetStarsi->getPageMargins()->setRight(0.5);
        $sheetStarsi->getPageMargins()->setBottom(1);
        $this->AdminStartListinaTemplate($sheetStarsi, 'STARŠÍ');
        $idx = 3;
        foreach(HasiciPohar::_Results_Get('starsi')  as $druzstvo) {
            $sheetStarsi->setCellValue('A'.$idx, $druzstvo['StartCislo']);
            $sheetStarsi->setCellValue('B'.$idx, $druzstvo['Druzstvo']);
            $idx++;
        }

        $sheetMladsi = $dokument->createSheet();;
        $sheetMladsi->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
        $sheetMladsi->getPageMargins()->setTop(1);
        $sheetMladsi->getPageMargins()->setLeft(0.5);
        $sheetMladsi->getPageMargins()->setRight(0.5);
        $sheetMladsi->getPageMargins()->setBottom(0.5);
        $this->AdminStartListinaTemplate($sheetMladsi, 'MLADŠÍ');
        $idx = 3;
        foreach(HasiciPohar::_Results_Get('mladsi')  as $druzstvo) {
            $sheetMladsi->setCellValue('A'.$idx, $druzstvo['StartCislo']);
            $sheetMladsi->setCellValue('B'.$idx, $druzstvo['Druzstvo']);
            $idx++;
        }

        $writer = new Xlsx($dokument);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment; filename="StartListina.xlsx"');
        $writer->save("php://output");
    }
    /*
    *   ŠTAFETA 4x60
    */
    public function index4x60()
    {
        return view('pohar.4x60', ['mladsi' => HasiciPohar::_4x60_Get('mladsi'), 'starsi' => HasiciPohar::_4x60_Get('starsi'), 'kategorie' => 'mladsi']);
    }
    public function store4x60(Request $request)
    {
        HasiciPohar::_4x60_Store($request->druzstvoID, $request->cas1, $request->cas2, $request->neplatny1, $request->neplatny2);
        $druzstvo = HasiciPohar::find($request->druzstvoID);
        if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('4x60_VyslednyCas')->count()==0) {
            HasiciPohar::_SpocitejPoradi_('4x60', $druzstvo->kategorie);
        }
        return view('pohar.4x60', ['mladsi' => HasiciPohar::_4x60_Get('mladsi'), 'starsi' => HasiciPohar::_4x60_Get('starsi'), 'kategorie' => $druzstvo->kategorie]);
    }
    /*
    *   ŠTAFETA DVOJIC
    */
    public function indexDvojice()
    {
        return view('pohar.dvojice', ['mladsi' => HasiciPohar::_Dvojice_Get('mladsi'), 'starsi' => HasiciPohar::_Dvojice_Get('starsi'), 'kategorie' => 'mladsi']);
    }
    public function storeDvojice(Request $request)
    {
        HasiciPohar::_Dvojice_Store($request->druzstvoID, $request->cas1, $request->cas2, $request->neplatny1, $request->neplatny2);
        $druzstvo = HasiciPohar::find($request->druzstvoID);
        if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Dvojice_VyslednyCas')->count()==0) {
            HasiciPohar::_SpocitejPoradi_('Dvojice', $druzstvo->kategorie);
        }
        return view('pohar.dvojice', ['mladsi' => HasiciPohar::_Dvojice_Get('mladsi'), 'starsi' => HasiciPohar::_Dvojice_Get('starsi'), 'kategorie' => $druzstvo->kategorie]);
    }
    /*
    *   POŽÁRNÍ ÚTOK
    */
    public function indexUtok()
    {
        return view('pohar.utok', ['mladsi' => HasiciPohar::_Utok_Get('mladsi'), 'starsi' => HasiciPohar::_Utok_Get('starsi'), 'kategorie' => 'mladsi']);
    }
    public function storeUtok(Request $request)
    {
        HasiciPohar::_Utok_Store($request->druzstvoID, $request->pokus, $request->cas, $request->neplatny);
        $druzstvo = HasiciPohar::find($request->druzstvoID);
        if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Utok_VyslednyCas')->count()==0) {
            HasiciPohar::_SpocitejPoradi_('Utok', $druzstvo->kategorie);
        }
        return view('pohar.utok', ['mladsi' => HasiciPohar::_Utok_Get('mladsi'), 'starsi' => HasiciPohar::_Utok_Get('starsi'), 'kategorie' => $druzstvo->kategorie]);
    }
    /*
    *   HADICOVÉ KOLO
    */
    public function indexKolo()
    {
        return view('pohar.hadic-kolo', ['mladsi' => HasiciPohar::_Kolo_Get('mladsi'), 'starsi' => HasiciPohar::_Kolo_Get('starsi'), 'kategorie' => 'mladsi']);
    }
    public function storeKolo(Request $request)
    {
        HasiciPohar::_Kolo_Store($request->druzstvoID, $request->cas, $request->neplatny);
        $druzstvo = HasiciPohar::find($request->druzstvoID);
        if (HasiciPohar::where('kategorie', $druzstvo->kategorie)->wherenull('Kolo_VyslednyCas')->count()==0) {
            HasiciPohar::_SpocitejPoradi_('Kolo', $druzstvo->kategorie);
        }
        return view('pohar.hadic-kolo', ['mladsi' => HasiciPohar::_Kolo_Get('mladsi'), 'starsi' => HasiciPohar::_Kolo_Get('starsi'), 'kategorie' => $druzstvo->kategorie]);
    }
}
