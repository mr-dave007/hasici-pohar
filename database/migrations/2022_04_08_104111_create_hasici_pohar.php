<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasici_pohars', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('kategorie', ['mladsi', 'starsi'])->nullable(false);
            $table->integer('StartCislo')->nullable(false);
            $table->string('Druzstvo', 100)->nullable(false);
            $table->integer('4x60_Cas1')->nullable(true)->default(null);
            $table->integer('4x60_Cas2')->nullable(true)->default(null);
            $table->dateTime('4x60_UkonceniCas')->nullable(true)->default(null);
            $table->integer('4x60_VyslednyCas')->nullable(true)->default(null);
            $table->integer('4x60_Poradi')->nullable(true)->default(null);
            $table->integer('Dvojice_Cas1')->nullable(true)->default(null);
            $table->integer('Dvojice_Cas2')->nullable(true)->default(null);
            $table->dateTime('Dvojice_UkonceniCas')->nullable(true)->default(null);
            $table->integer('Dvojice_VyslednyCas')->nullable(true)->default(null);
            $table->integer('Dvojice_Poradi')->nullable(true)->default(null);
            $table->dateTime('Kolo_UkonceniCas')->nullable(true)->default(null);
            $table->integer('Kolo_VyslednyCas')->nullable(true)->default(null);
            $table->integer('Kolo_Poradi')->nullable(true)->default(null);
            $table->integer('Utok_Cas1')->nullable(true)->default(null);
            $table->dateTime('Utok_UkonceniCas1')->nullable(true)->default(null);
            $table->integer('Utok_Cas2')->nullable(true)->default(null);
            $table->dateTime('Utok_UkonceniCas2')->nullable(true)->default(null);
            $table->integer('Utok_VyslednyCas')->nullable(true)->default(null);
            $table->integer('Utok_Poradi')->nullable(true)->default(null);
            $table->integer('CelkovySoucet')->nullable(true)->default(null);
            $table->integer('CelkovePoradi')->nullable(true)->default(null);
            $table->unique(['StartCislo', 'kategorie']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasici_pohars');
    }
};
